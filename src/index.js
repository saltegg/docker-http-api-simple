const debug = require('debug')('app')
const express = require('express')
const app = express()
const port = process.env.PORT || 3000

const {Docker} = require('node-docker-api'); 
const docker = new Docker({ socketPath: '/var/run/docker.sock' });

app.get('/', async (req, res) => {
    const list = await docker.container.list()
    return res.json(list.map(o=>o.data))
})


app.get('/:name', async (req, res) => {
    const container = (await docker.container.list()).find(c=>c.data.Names.includes(`/${req.params.name}`))
    return res.json(container.data)
})

app.get('/:name/restart', async (req, res) => {
    debug('restart',req.params.name)
    let container = (await docker.container.list()).find(c=>c.data.Names.includes(`/${req.params.name}`))
    await container.restart()
    container = (await docker.container.list()).find(c=>c.data.Names.includes(`/${req.params.name}`))
    return res.json(container.data)
})

app.listen(port, () => console.log(`App listening on port ${port}!`))
